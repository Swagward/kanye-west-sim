using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemCollection : MonoBehaviour
{
    int kiwis = 0;
    public AudioClip funny;

    [SerializeField] private Text kiwisText;

    void Start()
    {
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = funny;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Kiwi"))
        {
            Destroy(collision.gameObject);
            kiwis++;
            kiwisText.text = "Kiwi Count: " + kiwis;
            {
                //Debug.Log("Kiwi detected, changing count to " + kiwis);
            }
        }

        if (collision.gameObject.CompareTag("KSR"))
        {
            GetComponent<AudioSource>().Play();
            //Debug.Log("Bro i love kanye hes so pog");
        }
    }
} 